import pandas as pd
import datetime
import subprocess
import time
import threading
import json
import requests
import http
import numpy
import schedule
from requests.auth import HTTPDigestAuth
from multiprocessing.pool import ThreadPool, Pool
from pandas.util import hash_pandas_object

"""
    With ping timeout set to .025
    ~33 seconds if no online miners marked as 'NOS'
    ~6 minutes to add 215 S9's to the tracker as "new" machines
    ~6 minutes to add 125 S17+'s to tracker as "new" machines

    With ping timeout set to .005
    ~9 seconds if no online miners marked as 'NOS'
    ~5 minutes to add 122 S17+'s to tracker as "new" machines
"""

# 'ip address': 'row number in tracker' #
nos_machines = {}

# Miners that are online but show as NOS in tracker #
needs_info = {}

# Miners that are online/on shelf but are missing account/pool/coin etc. #
missing_info = {}
on_shelf = []

print('Monitoring started: ' + str(datetime.datetime.now().strftime('%x @ %X')))


# Variable to track changes made each loop
class Count:
    changes = 0


# Create dictionary of 'NOS' IP addresses and their row number in the tracker #
def nos_check():
    ont1 = pd.read_csv('/home/hbadmin/GoogleDrive/tracker.csv', dtype=object)
    ont = ont1.replace(numpy.nan, '', regex=True)
    row = -1

    for ip in ont['IP Address']:
        row += 1
        if 'On Shelf' not in ont.at[row, 'Status']:
            nos_machines[ip] = row
    return


def info_check():
    ont1 = pd.read_csv('/home/hbadmin/GoogleDrive/tracker.csv', dtype=object)
    ont = ont1.replace(numpy.nan, '', regex=True)
    row = -1

    for ip in ont['IP Address']:
        row += 1
        if 'Antminer' in ont.at[row, 'machineType'] and 'Self Mining' in ont.at[row, 'Hosted?'] and 'On Shelf' in ont.at[row, 'Status']:
            if ont.at[row, 'accountName'] == '' or ont.at[row, 'pool'] == '' or ont.at[row, 'coinMined'] == '':
                missing_info[ip] = row
    return


# Ping list of 'NOS' ip's, return online IP's to new dictionary with row numbers #
def ping(host, ont):
    # 'host' == ip address #
    url = 'http://' + host

    try:
        s = requests.Session()
        r = s.post(url, auth=HTTPDigestAuth('root', 'root'), timeout=.005)

        # Check to see if online machine is marked NOS - Cleaning or NOS - Empty #
        try:
            if r.status_code != 401:
                if ont.at[nos_machines[host], 'Status'] == 'NOS - Cleaning':
                    ont.at[nos_machines[host], 'Status'] = 'On Shelf'
                    Count.changes += 1
                    on_shelf.append(nos_machines[host])
                else:
                    if ont.at[nos_machines[host], 'machineType'] == '':
                        needs_info[host] = nos_machines[host]
        except Exception as e:
            print('Failed to add ' + host + ' to list - ' + str(e))

    except Exception as e:
        pass
    return


# Update tracker for machines online but marked 'NOS'
def get_info(host, ont, z):
    try:
        s = requests.Session()
        statusURL = 'http://' + host + '/cgi-bin/get_miner_status.cgi'
        infoURL = 'http://' + host + '/cgi-bin/get_system_info.cgi'

        statusResponse = s.post(statusURL, auth=HTTPDigestAuth('root', 'root'))
        infoResponse = s.post(infoURL, auth=HTTPDigestAuth('root', 'root'))

        statusData = json.loads(statusResponse.text)
        infoData = json.loads(infoResponse.text)

        try:
            if 'BCH' in statusData['pools'][0]['url']:
                coin = 'BCH'
            else:
                coin = 'BTC'

        except Exception as e:
            print('Failed to get coin type for ' + host + ' defaulting to BTC')
            coin = 'BTC'

        try:
            if ont.at[needs_info[host], 'serialNumber'] == '':
                ont.at[needs_info[host], 'serialNumber'] = infoData['macaddr']
            if ont.at[needs_info[host], 'machineType'] == '':
                ont.at[needs_info[host], 'machineType'] = infoData['minertype']
            if ont.at[needs_info[host], 'coinMined'] == '':
                ont.at[needs_info[host], 'coinMined'] = coin
            if ont.at[needs_info[host], 'Hosted?'] == '':
                ont.at[needs_info[host], 'Hosted?'] = 'Self Mining'
            if ont.at[needs_info[host], 'deployDate'] == '':
                ont.at[needs_info[host], 'deployDate'] = z
            if ont.at[needs_info[host], 'RMA?'] == '':
                ont.at[needs_info[host], 'RMA?'] = '0'
            if 'NOS' or '' in ont.at[needs_info[host], 'Status']:
                ont.at[needs_info[host], 'Status'] = 'On Shelf'

            try:
                ont.at[needs_info[host], 'pool'] = statusData['pools'][0]['url']
                ont.at[needs_info[host], 'accountName'] = statusData['pools'][0]['user'].split('.')[0]

            except Exception as e:
                print('Failed to get account/pool for ' + host)
            Count.changes += 1

        except Exception as e:
            print('Failed to update tracker for ' + host + ' - ' + str(e))

    except Exception as e:
        print('Failed to do anything for ' + host + ' - ' + str(e))

    return


def get_missing_info(host, ont):
    try:
        s = requests.Session()
        statusURL = 'http://' + host + '/cgi-bin/get_miner_status.cgi'
        infoURL = 'http://' + host + '/cgi-bin/get_system_info.cgi'

        statusResponse = s.post(statusURL, auth=HTTPDigestAuth('root', 'root'))
        infoResponse = s.post(infoURL, auth=HTTPDigestAuth('root', 'root'))

        statusData = json.loads(statusResponse.text)
        infoData = json.loads(infoResponse.text)

        try:
            if 'BCH' in statusData['pools'][0]['url']:
                coin = 'BCH'
            else:
                coin = 'BTC'

        except Exception as e:
            print('Failed to get coin type for ' + host + ' defaulting to BTC')
            coin = 'BTC'

        try:
            if ont.at[missing_info[host], 'machineType'] == '':
                ont.at[missing_info[host], 'machineType'] = infoData['minertype']
            if ont.at[missing_info[host], 'coinMined'] == '':
                ont.at[missing_info[host], 'coinMined'] = coin
            try:
                ont.at[missing_info[host], 'pool'] = statusData['pools'][0]['url']
                ont.at[missing_info[host], 'accountName'] = statusData['pools'][0]['user'].split('.')[0]

            except Exception as e:
                print('Failed to get account/pool for ' + host)
            Count.changes += 1

        except Exception as e:
            print('Failed to update tracker for ' + host + ' - ' + str(e))

    except Exception as e:
        print('Failed to do anything for ' + host + ' - ' + str(e))

    return


def run():
    start = time.time()
    x = datetime.datetime.now()
    z = x.strftime("%m/%d/%Y")

    info_check()
    nos_check()
    ont1 = pd.read_csv('/home/hbadmin/GoogleDrive/tracker.csv', dtype=object)
    ont = ont1.replace(numpy.nan, '', regex=True)
    hash_ont = hash_pandas_object(ont).sum()

    try:
        for ip in nos_machines:
            threading.Thread(ping(host=ip, ont=ont))
    except Exception as e:
        print('unable to start threading for pinging NOS machines')

    try:
        for ip in needs_info:
            threading.Thread(get_info(host=ip, ont=ont, z=z))
    except Exception as e:
        print('unable to log into online miners to get info')

    try:
        for ip in missing_info:
            threading.Thread(get_missing_info(host=ip, ont=ont))
    except Exception as e:
        print('unable to start threading for updating missing acc/pool/coins')

    # If no changes are made, don't save
    if Count.changes == 0:
        pass

    # If changes were made, save
    elif Count.changes > 0:
        try:
            ont.to_csv('/home/hbadmin/GoogleDrive/tracker.csv', index=False, encoding='utf-8')
            print(x.strftime('%x @ %X'))
            if len(on_shelf) > 0:
                print("Number of status updates: " + str(len(on_shelf)))

            if Count.changes > len(on_shelf):
                print('Number of other changes: ' + str(Count.changes - len(on_shelf)))
                print('Info changed on these machines: ')
                for i in needs_info:
                    print(i)
                for i in missing_info:
                    print(i)

            print('time elapsed: ' + str(int(time.time() - start)) + ' seconds\n')

        except Exception as e:
            if 'Permission denied' in str(e):
                print("failed to write to tracker: tracker.csv is open\nWaiting 5 seconds and trying again..")

                # Wait 5 sec for user to close tracker and try again, up to 5 tries
                tries = 1
                while tries < 6:
                    time.sleep(5)
                    print('try #' + str(tries + 1))

                    try:
                        ont.to_csv('/home/hbadmin/GoogleDrive/tracker.csv', index=False, encoding='utf-8')
                        print(x.strftime('%x @ %X'))
                        if len(on_shelf) > 0:
                            print("Number of status updates: " + str(len(on_shelf)))

                        if Count.changes > len(on_shelf):
                            print('Number of other changes: ' + str(Count.changes - len(on_shelf)))
                            print('Info changed on these machines: ')
                            for i in needs_info:
                                print(i)
                            for i in missing_info:
                                print(i)

                        print('time elapsed: ' + str(int(time.time() - start)) + ' seconds\n')

                    except Exception as e:
                        tries += 1
                        continue

            else:
                print(str(e))

    Count.changes = 0
    nos_machines.clear()
    needs_info.clear()
    missing_info.clear()

while True:
    run()
